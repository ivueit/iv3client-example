﻿using System;
using Json.Net;

using IV3Client;

namespace iv3client_example
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Connecting to iVueit...");
      IVueItAPIClient client = new IVueItAPIClient("sandbox.api.ivueit.network", "api-b3e7ff20-ead3-49df-84d6-bed2fc2b37d8", "ENEhAeyJTVZldEmIqwfwNJpkOrZilp9VZm9e2Y7YfGY6CEGx46j8PK9y7bqGLci43ZsAn0utHLjg5X2b4N89FN6k2L9HvmugEwoiKxpU8dbdbsSELdO0TIlMc6800PoN");

      var paginatedVues = client.GetVues(DateTime.UtcNow.AddDays(-100), DateTime.UtcNow);
      Console.WriteLine(paginatedVues.PageMeta.ToString());

      foreach (var vue in paginatedVues.Vues)
      {
        string vueJSONString = JsonNet.Serialize(vue);
        Console.WriteLine("Downloading file for vue (" + vue.CanonicalId + ") \n" + vueJSONString);

        if (vue.PDFFileId.Length > 0)
        {
          Console.WriteLine(client.FetchPresignedFileURL(vue.PDFFileId));
          client.DownloadFile(vue.PDFFileId, "./");
        }
      }
    }
  }
}